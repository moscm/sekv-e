# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

project = 'sEKV-E'
copyright = '2024, Hung-Chi Han'
author = 'Hung-Chi Han, Vincente Carbon'
release = '1.1.5'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    'sphinx.ext.autosummary',
    'sphinx_autodoc_typehints',
    'sphinx.ext.viewcode',
    'sphinx.ext.githubpages',
    'nbsphinx',
    'sphinx_issues'
]

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

numpydoc_show_class_members = False
autosummary_generate = True
nbsphinx_allow_errors = True

issues_default_group_project = "moscm/sekv-e"
issues_uri = "https://gitlab.com/moscm/sekv-e/-/issues/{issue}"
issues_prefix = "#"
issues_pr_uri = "https://gitlab.com/moscm/sekv-e/-/merge_requests/{pr}"
issues_pr_prefix = "!"
issues_commit_uri = "https://gitlab.com/moscm/sekv-e/-/commit/{commit}"
issues_commit_prefix = "@"
issues_user_uri = "https://gitlab.com/{user}"
issues_user_prefix = "@"
