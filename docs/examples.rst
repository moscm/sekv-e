.. _examples:

Examples
=========

.. toctree::
   :maxdepth: 1
   :caption: Jupyter Notebooks

   examples/15_minutes_to_sekve.ipynb
   examples/extracting_output_conductance_parameters.ipynb