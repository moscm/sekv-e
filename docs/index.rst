.. sEKV-E documentation master file, created by
   sphinx-quickstart on Mon Jul 22 09:43:16 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SEKV-E documentation
====================

SEKV-E is a Python-based parameter extractor developed by ICLAB, EPFL. The tool is developed for extracting parameters
of simplified EKV (sEKV) MOSFET model, which is a design-oriented model for low-power analog circuit designs.

The goal is providing a useful tool, enabling the sEKV modeling of MOSFET performance, so:

- Analog designers don't need to spend a long time learning the sEKK model and its extraction methodology.

- Provide a solution of integrating circuit designs in Python language.

- Streamline the circuit design and enable the screening of various silicon technologies.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   modules
   examples
   changes/index

.. automodule:: sekve
   :members:
   :undoc-members:
   :show-inheritance:

Indices and tables
==================


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
