Usage
=====
Requirements
------------

You need a working python 3.7 installation, as the minimum Python version,
to be able to use sEKV-E. We highly recommend installing Miniconda, which takes care of installing Python
and managing packages. In the following it will be assumed that you use Miniconda.
Download and install it from `here <https://docs.conda.io/en/latest/miniconda.html>`_.

Installation
------------

Stable versions of SEKV-E are distributed via both PyPi and CondaForge to be installed with pip and conda, respectively.
Below we will cover both installation types.

Installing the latest SEKV-E release with pip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Launch a prompt and activate an environment if needed.

Type in the prompt:

.. code:: bash

   conda activate your_env
   pip install sekve

The first line activate an existing conda environment and second line installs the package.

Installing the latest SEKV-E release with conda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Type in the prompt:

.. code:: bash

   conda activate your_env
   conda install -c conda-forge sekve

The second line installs the package from the conda-forge channel.

Updating SEKV-E
~~~~~~~~~~~~~~~

If you have installed with pip, run the following to update:

.. code:: bash

   pip install --upgrade qcodes

If you have installed from conda-forge

.. code:: bash

   conda update qcodes
