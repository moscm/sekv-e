import argparse
import os
import re

running_path:str = os.path.dirname(os.path.realpath(__file__))

def change_name_version(file_path:str,change_function,new_name:str,new_version:str)->None:
    with open(file_path,'r') as file:
        initial_string = file.read()
    new_string = change_function('name', new_name, initial_string)
    new_string = change_function('version', new_version, new_string)
    with open(file_path,'w') as file:
        file.write(new_string)

def change_meta_variable(variable_name:str,new_value:str,full_file:str):
    rexp_to_find = r"\{% set " + variable_name + r".* %\}"
    substitution = f'{{% set {variable_name} = "{new_value}" %}}'
    new_file = re.sub(rexp_to_find, substitution, full_file)
    return new_file

def change_setup_variable(variable_name:str,new_value:str,full_file:str):
    rexp_to_find = variable_name + r"=.*,"
    substitution = f'{variable_name}="{new_value}",'
    new_file = re.sub(rexp_to_find, substitution, full_file)
    return new_file

def change_test_variable(variable_name:str,new_value:str,full_file:str):
    rexp_to_find = variable_name + r"=.*"
    substitution = f'{variable_name}="{new_value}"'
    new_file = re.sub(rexp_to_find, substitution, full_file)
    return new_file

def main()->None:
    # Instantiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument('name', type=str,
                        help='"name" in the meta file')
    parser.add_argument('version', type=str,
                        help='"version" in the meta file')

    args = parser.parse_args()

    meta_file_path:str = os.path.join(running_path, "conda_compilation", "meta.yaml")
    change_name_version(meta_file_path, change_meta_variable,args.name,args.version)

    setup_file_path:str = os.path.join(os.path.dirname(running_path),"setup.py")
    change_name_version(setup_file_path, change_setup_variable,args.name,args.version)
    

if __name__ == '__main__':
    main()
