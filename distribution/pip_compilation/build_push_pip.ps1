# Automatisation process pip


param(
    [Parameter(Mandatory=$true)]
    [string]$version
    )
$name = "sekve"

# # Functions

function Ask-Question {
    param (
        [string] $Question,
        [string[]] $PossibleAnswers
    )

    $Possible1 = $PossibleAnswers[0]
    $Possible2 = $PossibleAnswers[1]
    $Question = "$Question ($Possible1 or $Possible2)"
    while (1) {
        $Answer = Read-Host -Prompt $Question
        if (($Answer -eq "$Possible1") -Or ($Answer -eq "$Possible2")) 
        {
            break
        }
        Write-Host 'Invalid input.'
    }
    return $Answer
}

function Exit-Script {
    Set-Location "$Initial_Directory"
    exit
}

# Push new version on Git
$Initial_Directory = Get-Location
Set-Location "$PSScriptRoot\..\.."
if ($(git tag -l --contains origin/main $version)) 
{
    Write-Output "Version already exists on Git for origin/main. Push new version."
    Exit-Script
}
if ($(pip install "$name==" | Select-String $version)) {
    Write-Output "Version already exists on PyPI."
}

python ""$PSScriptRoot"\..\change_name_version.py" $name $version # Update varaibles on meta files

$upload = Ask-Question "Upload files?" "y","n"

if ($upload -eq "n") {
    Remove-Item -Path ""$PSScriptRoot"\..\..\dist\*"
    Exit-Script
}

conda list
pip install wheel
python setup.py sdist bdist_wheel
pip uninstall wheel

# Have a PyPI account
pip install twine
twine upload dist/*
pip uninstall twine

Remove-Item -Path "dist\*"
Remove-Item -Path "build\*"
# git push origin $version
Exit-Script