param(
    [Parameter(Mandatory = $true)]
    [string]$version
    )
    
$name = "sekve"
PowerShell "$PSScriptRoot/pip_compilation/build_push_pip.ps1 $version"

pip install bs4
pip install requests
python ""$PSScriptRoot"\conda_compilation\update_meta_sha256.py" $name $version
pip uninstall bs4
pip uninstall requests

$a, $b, $c = $version -split "\."
$branch = "$a." + "$b" + ".x"

git commit -a -m "Updated meta file"
git checkout main
git merge --no-ff release/$branch
git tag -a $version -m "Added tag '$version'"
git push origin --tags
git push
git checkout develop
git merge --no-ff release/$branch
git push
git checkout main
Write-Output "New tag created on Git with name: "$version""

$path_git_forge = Read-Host -Prompt "Path of conda-forge local git main folder: "

Remove-Item -Path "$path_git_forge\recipe\meta.yaml"
Copy-Item -Path "$PSScriptRoot\conda_compilation\meta.yaml" -Destination "$path_git_forge\recipe\meta.yaml"
Set-Location $path_git_forge
git checkout $name
git add "recipe\meta.yaml"
git commit -m "New meta file"
git push

# Create pull request
