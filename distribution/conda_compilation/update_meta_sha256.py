import argparse
import os
import re
import requests
from bs4 import BeautifulSoup

URL= "https://pypi.org/project/sekve/"
running_path:str = os.path.dirname(os.path.realpath(__file__))

def change_meta_sha256(new_value:str,full_file:str):
    rexp_to_find = r"(?<=sha256: ).*"
    substitution = new_value
    new_file = re.sub(rexp_to_find, substitution, full_file)
    return new_file

def main():
    
    # Instantiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument('name', type=str,
                        help='"name" in the meta file')
    parser.add_argument('version', type=str,
                        help='"version" in the meta file')

    args = parser.parse_args()
    name = args.name
    version = args.version


    response  = requests.get(URL+"#files")
    if not response.ok:
        exit()

    soup = BeautifulSoup(response.text, 'html.parser')
    sha256_link = soup.find("a", text=re.compile(f"{name}-{version}.tar.gz")).find_next("a")['href']

    response  = requests.get(URL+sha256_link)

    if not response.ok:
        exit()

    soup = BeautifulSoup(response.text, 'html.parser')
    sha256  = soup.find("caption", text=re.compile(f"{name}-{version}.tar.gz")).parent.find("th", text=re.compile("SHA256")).parent.find("code").text

    
    file_path:str = os.path.join(running_path, "meta.yaml")
    
    with open(file_path,'r') as file:
        initial_string = file.read()
    new_string = change_meta_sha256(sha256, initial_string)
    with open(file_path,'w') as file:
        file.write(new_string)



if __name__ == '__main__':
    main()


